function addToCart(ctx) {
    let {context, data} = ctx;
    let {burgerType, quantity} = context;

    if (burgerType && quantity) {
        if (data.cart === undefined) data.cart = [];
        data.cart.push({burgerType, quantity});
    }

    return ctx;
}